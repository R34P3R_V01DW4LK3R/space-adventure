//
//  SpaceAdvanture.swift
//  SpaceAdventure
//
//  Created by GDD Student on 19/4/16.
//  Copyright © 2016 Your School. All rights reserved.
//

import Foundation
import GameplayKit

class SpaceAdventure{
    let planetarySystem:PlanetarySystem
    init(planetarySystem: PlanetarySystem){
        self.planetarySystem = planetarySystem
        
    }
    
    //Public/internal
    private func displayintroduction(){
        //let numberOfPlanets = 8
        let diameterOfEarth = 24859.82 // In miles, from pole to pole.
        
        //print introduction
        print("Welcome to our \(planetarySystem.name)")
        print("There are \(/*planetarySystem.numberOfPlanets*/planetarySystem.planets.count) planets to explore.")
        print("Your are currently on Earth, which has a circumference of \(diameterOfEarth) miles.")

    }
    private func greetplayer(){
        //greet the player
        //print("What is your name")
        //let name = getln()
       
        let name = responseToPrompt("what is your name")
        
        print("nice to meet you \(name),My name is Cortana. i am a friend of siri")
    }
    
    private func responseToPrompt(prompt: String)->String{
        print(prompt)
        return getln()
    }
    
    private func DetermineDestination(){
        var decision = ""
        while (decision != "Y" && decision != "N"){
            
            decision = responseToPrompt("shall 1 randomly choose a planet for you to visit? (Y or N)")
            
            if (decision == "Y") {
                print ("Okay! Travel to .../")
                if let planet = planetarySystem.randomPlanet{
                    visit(planet.name)
                } else {
                    print("I 'm sorry,this planetary system has no planet to visit")
                }
            } else if (decision == "N") {
               let planetName = responseToPrompt("Ok, name the planet you would like to visits...")
                visit(planetName)
            } else {
                print("Sorry, I didn't get that")
            }
        }
        

    }
    
    private func visit(PlanetName:String){
        print("Traveling to \(PlanetName).....")
        for var i=0; i < planetarySystem.planets.count; i++ {
            let planet = planetarySystem.planets[i]
            if PlanetName == planet.name{
                print("Arrived at \(planet.name).\(planet.description)")
            }
        }
        
    }
    
    
    func start()
    {
        displayintroduction()
        greetplayer()
        //planet choosing section
        if (!planetarySystem.planets.isEmpty){
            print("Let's go on an adventure!")
            DetermineDestination()
            
            print ("Starting your adventure!")
        }
       
    }
  
}
