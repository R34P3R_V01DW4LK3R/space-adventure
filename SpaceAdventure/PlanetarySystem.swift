//
//  PlanetarySystem.swift
//  SpaceAdventure
//
//  Created by GDD Student on 21/4/16.
//  Copyright © 2016 Your School. All rights reserved.
//

import Foundation
import GameplayKit

class PlanetarySystem {
    let name: String
    //let numberOfPlanets : Int
    
    var planets : [Planet]
    
    init(name: String, /*numberOfPlanets: Int*/planets: [Planet]){
    self.name = name
    //self.numberOfPlanets = numberOfPlanets
    self.planets = planets
    }
    
    var randomPlanet: Planet?{
        if planets.isEmpty{
            return nil
        } else {
            let rand = GKRandomSource.sharedRandom()
            let chosen = rand.nextIntWithUpperBound(planets.count)
            
            return planets[chosen]
            }
        }
    }


class Planet{
    let name: String
    let description: String
    
    init(name: String,description: String){
        self.name=name
        self.description=description
    }
}