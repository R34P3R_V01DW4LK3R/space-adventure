/*

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike
4.0 International License, by Yong Bakos.

*/

import Foundation

let planetData = [
 "Mercury": "A Very Hot ,closest to the sun.",
 "Venus": "sounds nice with uranus",
 "Mars": "there is a nice stash of bars there",
 "Earth": "third rock from the sun",
 "Neptune": "Just water and ice",
 "Jupiter": "the largest planet",
 "Uranus": "sounds nice with venus",
 "Saturn": "ring around the rosy",
 "Pluto": "Mickey's dog",
]

var planets = planetData.map { name, description in
    Planet(name: name, description: description )
}

let x = PlanetarySystem(name: " Solar System",/*numberOfPlanets: 8*/planets: planets)


let adventure = SpaceAdventure(planetarySystem: x)
adventure.start()